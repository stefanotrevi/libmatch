#include "aho_korasick.hpp"
#include "measure.hpp"
#include "naive.hpp"
#include <iostream>

static bool run_tests()
{
    auto texts = std::to_array<std::string>({
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum fermentum "
        "eros "
        "blandit dapibus. Donec placerat ultrices volutpat. Phasellus non est nec turpis "
        "viverra "
        "commodo id at arcu. Nulla interdum nulla non felis sagittis, a fringilla justo "
        "fringilla. "
        "Aliquam porttitor magna eget metus aliquam accumsan. Suspendisse risus nisi, tempus "
        "vitae "
        "purus in, convallis ornare tellus. Nunc sodales maximus tortor, non gravida risus "
        "porttitor sit amet. Praesent at sem eu tellus tincidunt fringilla non id mauris. "
        "Donec "
        "ut "
        "dapibus tortor, eget viverra arcu. Vestibulum ante ipsum primis in faucibus orci "
        "luctus "
        "et ultrices posuere cubilia curae; Integer non gravida nibh. Aenean blandit diam at "
        "fermentum auctor. Curabitur dignissim commodo orci malesuada pretium. Maecenas mollis "
        "sed "
        "nibh ultricies cursus.",
        //
        "Quo usque tandem abutere, Catilina, patientia nostra? Quam diu etiam furor iste tuus "
        "nos eludet? Quem ad finem sese effrenata iactabit audacia? Nihilne te nocturnum "
        "praesidium Palati, nihil urbis vigiliae, nihil timor populi, nihil concursus "
        "bonorum omnium, nihil hic munitissimus habendi senatus locus, nihil horum ora "
        "voltusque moverunt? Patere tua consilia non sentis, constrictam iam horum omnium "
        "scientia teneri coniurationem tuam non vides? Quid proxima, quid superiore nocte "
        "egeris, ubi fueris, quos convocaveris, quid consilii ceperis, quem nostrum ignorare "
        "arbitraris? O tempora, o mores! Senatus haec intellegit. Consul videt; hic "
        "tamen vivit. Vivit? Immo vero etiam in senatum venit, fit publici consilii particeps, "
        "notat et designat oculis ad caedem unum quemque nostrum. Nos autem fortes viri satis "
        "facere rei publicae videmur, si istius furorem ac tela vitemus. Ad mortem te, "
        "Catilina, duci iussu consulis iam pridem oportebat, in te conferri pestem, quam tu in "
        "nos "
        "[omnes iam diu] machinaris. An vero vir amplissumus, P. Scipio, pontifex maximus, "
        "Ti. Gracchum mediocriter labefactantem statum rei publicae privatus interfecit; "
        "Catilinam "
        "orbem terrae caede atque incendiis vastare cupientem nos consules perferemus? Nam "
        "illa "
        "nimis antiqua praetereo, quod C. Servilius Ahala Sp. Maelium novis rebus studentem "
        "28 manu sua occidit. Fuit, fuit ista quondam in hac re publica virtus, ut viri fortes "
        "acrioribus suppliciis civem perniciosum quam acerbissimum hostem coercerent. Habemus "
        "senatus consultum in te, Catilina, vehemens et grave, non deest rei publicae "
        "consilium "
        "neque auctoritas huius ordinis; nos, nos, dico aperte, consules desumus.",
        //
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        //
        random_string(1 << 18, "01"),
    });
    auto patterns = std::to_array<std::string>(
        {"it", "et", "nos", "mus", "nostr", "1001010100110110", "aaaaaaaaaaaaaaaaaa"});
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Matching... ";
    check = true;
    for (size_t i = 0; i < texts.size(); ++i)
    {
        auto m1{match::aho_korasick(texts[i], patterns)};
        auto m2{match::naive_multi(texts[i], patterns)};
        check &= m1 == m2;

        for (size_t i = 0; i < std::min(m1.size(), m2.size()); ++i)
            std::cout << m1[i] << " ... " << m2[i] << '\n';
        std::cout << '\n';
    }
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing Aho-Korasick matcher ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

#ifdef MEASURE_PERFORMANCE
    std::string text(1 << 20, 'a');
    std::string pattern(1 << 12, 'a');

    std::cout << "Measuring performance...\n";
    measure([&]() { match::aho_korasick(text, std::to_array<std::string>({pattern})); }, 4, 4);
#endif

    return 0;
}