#include "keyword_tree.hpp"
#include <iostream>
#include <array>

static bool run_tests()
{
    auto patterns = std::to_array<std::string>({"carrot", "cartone", "mela", "pera", "persona",
                                                "metafora", "pianoforte", "altezza", "albicocca"});
    bool check = true;
    bool all_check = true;
    match::KeywordTree tree{patterns};

    std::cout << std::boolalpha;

    std::cout << "Tree construction... ";
    check = true;
    std::cout << tree << '\n';
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Tree sorting... ";
    check = true;
    tree.sort();
    std::cout << tree << '\n';
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing KeywordTree ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

#ifdef MEASURE_PERFORMANCE // maybe some lookup tests, idk
#endif

    return 0;
}
