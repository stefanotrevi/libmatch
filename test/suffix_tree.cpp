#include "suffix_tree.hpp"
#include <iostream>

static bool run_tests()
{
    std::string str{"axabxb"};

    bool check = true;
    bool all_check = true;
    match::SuffixTree tree{str};

    std::cout << std::boolalpha;

    std::cout << "Tree construction... ";
    check = true;
    std::cout << tree << '\n';
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Tree sorting... ";
    check = true;
    tree.sort();
    std::cout << tree << '\n';
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing SuffixTree ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

#ifdef MEASURE_PERFORMANCE // maybe some lookup tests, idk
#endif

    return 0;
}
