#include "suffix_trie.hpp"
#include <iostream>
#include <array>

static bool run_tests()
{
    auto text{"cioccolata"};
    bool check = true;
    bool all_check = true;
    match::SuffixTrie trie{text};

    std::cout << std::boolalpha;

    std::cout << "Trie construction... ";
    check = true;
    std::cout << trie << '\n';
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Trie sorting... ";
    check = true;
    trie.sort();
    std::cout << trie << '\n';
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing SuffixTrie ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

#ifdef MEASURE_PERFORMANCE // maybe some lookup tests, idk
#endif

    return 0;
}
