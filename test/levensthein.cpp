#include "levensthein.hpp"
#include "measure.hpp"
#include "string_utils.hpp"

#include <iostream>

static bool run_tests()
{
    std::string str = "dimssidmsiddsimdmsmmm";
    uint64_t real_cost = 14;

    bool all_check = true;
    bool check = true;

    std::cout << std::boolalpha;

    std::cout << "Levensthein v0... ";
    check = align::levensthein0(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Levensthein v1... ";
    check = align::levensthein1(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Levensthein v2... ";
    check = align::levensthein2(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Levensthein v3... ";
    check = align::levensthein3(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Levensthein no_check... ";
    check = align::levensthein_nocheck(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Levensthein no_check v2... ";
    check = align::levensthein_nocheck_v2(str) == real_cost;
    all_check &= check;
    std::cout << check << '\n';

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing Levensthein distance ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    std::string text{random_string(1 << 20, "dims")};
#ifdef MEASURE_PERFORMANCE
    uint64_t cost;

    std::cout << "Measuring performance v0...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein1(text); }, 64, 4);
    std::cout << cost << '\n';

    std::cout << "Measuring performance v1...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein1(text); }, 64, 4);
    std::cout << cost << '\n';

    std::cout << "Measuring performance v2...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein2(text); }, 64, 4);
    std::cout << cost << '\n';

    std::cout << "Measuring performance v3...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein3(text); }, 64, 4);
    std::cout << cost << '\n';

    std::cout << "Measuring performance v4...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein_nocheck(text); }, 64, 4);
    std::cout << cost << '\n';

    std::cout << "Measuring performance v5...\n";
    cost = 0;
    measure([&]() { cost += align::levensthein_nocheck_v2(text); }, 64, 4);
    std::cout << cost << '\n';

#endif

    return 0;
}
