#include "align.hpp"
#include "levensthein.hpp"
#include "measure.hpp"
#include "string_utils.hpp"

#include <iostream>

static bool run_tests()
{
    auto text{std::to_array<std::pair<std::string, std::string>>({
        {"agtcg", "agtcg"},
        {"agtcg", "agcta"},
        {"casa", "cresta"},
        {"gabibbo", "giubbino"},
        {"fischietto", "pistacchio"},
    })};
    std::array best_align{
        // not necessarily unique!
        "mmmmm", "mmsss", "mismim", "mssdmmiim", "smmiiimmmdddm",
    };

    bool all_check = true;
    bool check = true;

    std::cout << std::boolalpha;

    std::cout << "Exponential... ";
    check = true;
    for (size_t i = 0; i < text.size(); ++i)
    {
        std::string my_align{align::exponential(text[i].first, text[i].second)};
        uint64_t my_cost = align::levensthein(my_align);
        uint64_t best_cost = align::levensthein(best_align[i]);
        std::string res = align::apply(text[i].first, text[i].second, my_align);

        check &= (res == text[i].second) & (my_cost <= best_cost);
        std::cout << text[i] << " -> " << my_align << " -> " << res << '\n';
    }
    all_check &= check;
    std::cout << check << '\n';

    std::cout << "Dynamic... ";
    check = true;
    for (size_t i = 0; i < text.size(); ++i)
    {
        std::string my_align{align::dynamic(text[i].first, text[i].second)};
        uint64_t my_cost = align::levensthein(my_align);
        uint64_t best_cost = align::levensthein(best_align[i]);
        std::string res = align::apply(text[i].first, text[i].second, my_align);

        check &= (res == text[i].second) & (my_cost <= best_cost);
        std::cout << text[i] << " -> " << my_align << " -> " << res << '\n';
    }
    all_check &= check;
    std::cout << check << '\n';


    return all_check;
}

int main()
{
    std::cout << "\n==== Testing String Alignment ====\n";

    bool all_check = run_tests();

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

#ifdef MEASURE_PERFORMANCE
    {
        std::string text1{random_string(9, "acgt")};
        std::string text2{random_string(9, "acgt")};

        std::cout << "Measuring exponential performance (match 9 characters)...\n";
        measure([&]() { align::exponential(text1, text2); }, 4, 4);
    }
    {
        std::string text1{random_string(1 << 10, "acgt")};
        std::string text2{random_string(1 << 10, "acgt")};

        std::cout << "Measuring dynamic programming performance (match 1024 characters)...\n";
        measure([&]() { align::dynamic(text1, text2); }, 4, 4);
    }
#endif

    return 0;
}
