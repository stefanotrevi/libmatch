#include "suffix_tree.hpp"

#include <algorithm>
#include <iostream>
#include <queue>

namespace match // SuffixTreeNode
{
    SuffixTreeNode::SuffixTreeNode(SuffixTreeNode *dad, std::string_view e_lab) :
        father{dad}, depth{dad->depth + 1}, e_label{e_lab}
    {}

    void SuffixTreeNode::insert_no_check(std::string_view str) { child.emplace_front(this, str); }

    void SuffixTreeNode::extend_label(std::string_view::const_iterator it)
    {
        e_label = std::string_view{e_label.begin(), it};
    }
    bool SuffixTreeNode::is_last_son() const
    {
        return father == nullptr || this == &this->father->child.back();
    }

    void SuffixTreeNode::sort()
    {
        child.sort([](auto &&x, auto &&y) { return x.e_label < y.e_label; });

        for (auto &&c : child)
            c.sort();
    }

    SuffixTreeNode *SuffixTreeNode::find(std::string_view str)
    {
        auto son = std::ranges::find_if(child, [&](auto &&x) { return x.e_label == str; });

        return son == child.end() ? nullptr : &*son;
    }

    SuffixTreeNode *SuffixTreeNode::find(char c)
    {
        auto son = std::ranges::find_if(child, [&](auto &&x) { return x.e_label.starts_with(c); });

        return son == child.end() ? nullptr : &*son;
    }

    const SuffixTreeNode *SuffixTreeNode::find(std::string_view str) const
    {
        auto son = std::ranges::find_if(child, [&](auto &&x) { return x.e_label == str; });

        return son == child.end() ? nullptr : &*son;
    }

    const SuffixTreeNode *SuffixTreeNode::find(char c) const
    {
        auto son = std::ranges::find_if(child, [&](auto &&x) { return x.e_label.starts_with(c); });

        return son == child.end() ? nullptr : &*son;
    }

    bool SuffixTreeNode::contains(std::string_view str) const { return this->find(str) != nullptr; }

    bool SuffixTreeNode::contains(char c) const { return this->find(c) != nullptr; }

    void SuffixTreeNode::insert(std::string_view str)
    {
        if (!this->contains(str))
            this->insert_no_check(str);
    }

    bool SuffixTreeNode::is_leaf() const { return child.empty(); }

    bool SuffixTreeNode::is_output() const { return label != NOT_OUTPUT_LABEL; }

    size_t SuffixTreeNode::get_depth() const { return depth; }

    size_t SuffixTreeNode::get_label() const { return label; }

    const SuffixTreeNode *SuffixTreeNode::slink() const { return suf_link; }

    std::ostream &operator<<(std::ostream &os, const SuffixTreeNode &node)
    {
        if (node.is_output())
            os << ' ' << node.label;
        os << '\n';

        for (auto &&c : node.child)
        {
            for (size_t i = node.depth - 1; i < node.depth; --i)
            {
                const SuffixTreeNode *aux = &node;

                for (size_t j = 0; j < i; ++j)
                    aux = aux->father;
                os << (aux->is_last_son() ? "  " : "| ");
            }
            os << c.e_label << " \\" << c;
        }

        return os;
    }
} // namespace match

namespace match // SuffixTree
{
    SuffixTree::SuffixTree(std::string_view in_str) : data{str}, str{data}
    {
        /*
        A suffix trie is a compressed suffix trie, where cosecutive non-branching edges are merged 
        into a single edge, and are labeled with a string_view, to take space O(1). 
        This means that the tree must save a local copy of the string, to avoid label invalidation.
        We can build the suffix tree in linear time using Ukkonen's algorithm, which uses a 
        modified version of the algorithm to build a SuffixTrie.
        1. At every step i, we need a pointer to the last leaf output node (left), 
        and a pointer to the first internal output node which already has a child labeled with 
        str[i] (right). We need to work in the boundary path (i.e. the trail of suffix links)
        between left and right, inclusive.
        2. The edge label of the left node is extended to the end of the string, and never touched 
        again. Its suffix link becomes the new left node.
        */
        SuffixTreeNode *fout = &root;
        SuffixTreeNode *left = &root;
        SuffixTreeNode *right = nullptr;

        for (size_t i = 0; i < str.size(); ++i)
        {
            // extend last leaf until the end of the string
            left->extend_label(str.end());
            // update last leaf (it will be a leaf at the end of the loop)
            left = left->suf_link ? left->suf_link : &root;

            // travel between the active point and the end point
            for (auto node = left; node != right; node = node->suf_link)
            {
                left->insert_no_check(str.substr(i, 1));
            }
        }
    }

    SuffixTree::~SuffixTree()
    {
        // The default destructor is recursive and causes stack overflow for big trees
        for (SuffixTreeNode *node = &root, *father = &root;;)
        {
            while (!node->child.empty())
                node = &node->child.front();

            father = node->father;
            if (!father)
                break;
            father->child.pop_front();
            node = father;
        }
    }

    const SuffixTreeNode *SuffixTree::get_root() const { return &root; }

    void SuffixTree::sort() { root.sort(); }
    size_t SuffixTree::size() const { return num_nodes; }

    std::ostream &operator<<(std::ostream &os, const SuffixTree &tree)
    {
        return os << "\n\\" << *tree.get_root();
    }

} // namespace match
