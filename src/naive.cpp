#include "naive.hpp"

namespace match
{
    MatchVec naive(std::string_view txt, std::string_view pat)
    {
        /*
        The naive matching algorithm is very simple: for every i < txt.size(), if
        txt[i:i+pat.size()] == pat[:], then we have a match. 
        For "random" strings this is often the fastest algorithm, as it can easily be vectorized 
        and parallelized, requires no preprocessing, and it is in-place.
        But when the number of (possibly partial) matches is high, the complexity can grow up to 
        O(txt.size() * pat.size()).
        */

        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz; ++i)
            if (std::equal(pat.begin(), pat.end(), txt.begin() + i))
                matches.push_back(i);

        return matches;
    }
} // namespace match
