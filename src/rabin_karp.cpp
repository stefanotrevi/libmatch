#include "rabin_karp.hpp"
#include "avxlib.hpp"
#include <boost/multiprecision/miller_rabin.hpp>

#ifdef _WIN32
void boost::throw_exception(class std::exception const &, struct boost::source_location const &)
{
    exit(EXIT_FAILURE);
}
#endif

namespace match
{
    static uint64_t random_prime()
    {
        static constexpr uint64_t MAX_LIM = std::numeric_limits<uint64_t>::max() - MAX_ALPHABET_SZ;
        static std::mt19937_64 rng{std::random_device{}()};

        uint64_t p = rng() | 1; // it has to be odd

        while ((p <= MAX_ALPHABET_SZ) & (p >= MAX_LIM) &
               !boost::multiprecision::miller_rabin_test(p, 25))
            p += 2;

        return p;
    }

    static constexpr uint64_t pow_mod(uint64_t x, uint64_t n, uint64_t m)
    {
        uint64_t y = 1;

        while (n)
        {
            uint64_t l, h;

            if (n & 1)
            {
                l = _mulx_u64(y, x, (unsigned long long *)&h);
                _udiv128(h, l, m, &y);
            }

            l = _mulx_u64(x, x, (unsigned long long *)&h);
            _udiv128(h, l, m, &x);

            n >>= 1;
        }

        return y;
    }

    static inline uint64_t mul_mod(uint64_t x, uint64_t y, uint64_t m)
    {
        uint64_t lo = _mulx_u64(x, y, (unsigned long long*)&x);

        _udiv128(x, lo, m, &x);

        return x;
    }

    static inline uint64_t sub_mod(uint64_t x, uint64_t y, uint64_t m)
    {
        return x >= y ? x - y : m - (y - x);
    }

    MatchVec rabin_karp(std::string_view txt, std::string_view pat)
    {
        /*
        The Rabin-Karp algorithm is an arithmetic approach to pattern matching.
        If we were able to compare, in constant time, an arbitrary number of bits, then the naive 
        algorithm complexity becomes linear.
        Since this is clearly impossible, we hash the pattern, and then we hash, consecutively, 
        all the substrings of txt of length pat.size(); comparing the hashes will take constant 
        time, although the result will be probabilistic.
        The hash function must be designed in a way that:
            hash(txt[i+1:j+1]) == f(hash(txt[i:j]), txt[i], txt[j])
        so that hashes other than the first can be computed in constant time.
        In the Rabin-Karp algorithm, hash(txt[i:j]) = int(txt[i:j]) % p, for some arbitrary p.
        Using the Horner rule, and the linearity of modulo operations, computing the hashes 
        after the first one can be done in constant time. 
        */
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        /* q is always strictly bigger than MAX_ALPHA_SZ and strictly smaller than 
        MAX_U64 - MAX_ALPHA_SZ, to avoid corner cases in the modular operations
        */
        uint64_t q = random_prime();
        uint64_t max_dig = pow_mod(MAX_ALPHABET_SZ, pat.size() - 1, q);
        uint64_t pat_h = 0;
        uint64_t txt_h = 0;

        for (size_t i = 0; i < pat.size(); ++i)
        {
            pat_h = (mul_mod(pat_h, MAX_ALPHABET_SZ, q) + pat[i]) % q;
            txt_h = (mul_mod(txt_h, MAX_ALPHABET_SZ, q) + txt[i]) % q;
        }

        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz; ++i)
        {
            // if there is a hash match we also perform naive checking
            if (pat_h == txt_h && std::equal(pat.begin(), pat.end(), txt.begin() + i))
                matches.push_back(i);

            // txt_h = ((txt_h - txt[i] * max_dig) * MAX_ALPHABET_SZ
            txt_h = mul_mod(sub_mod(txt_h, mul_mod(txt[i], max_dig, q), q), MAX_ALPHABET_SZ, q);

            txt_h = (txt_h + txt[i + pat.size()]) % q;
        }

        return matches;
    }

    MatchVec rabin_karp_maxmod(std::string_view txt, std::string_view pat)
    {
        /* 
        In this version the modulo is fixed to 2^{N}, where N is the maximum number of bits 
         comparable in constant time, which 
         is 2^256 on an AVX-2 CPU, i.e. we can compare up to 32 bytes in constant time.
         Any substring whose last 32 bytes equal the last 32 bytes of the pattern will result in 
         a false-positive. 
         Some would consider this a bad behaviour, but in our use-case it implies that any 
         collision has always a suffix in common with the real pattern, also we can compare 
         much longer strings and much quicker than we would do if p was prime)
         */

        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        __m256i pat_h;

        // pattern size too small, zero out excess elements
        if (pat.size() < sizeof(pat_h))
        {
            uint32_t mask = ~0U >> (sizeof(pat_h) - pat.size());
            __m256i mask_v = avx::btob_no_norm(mask);

            pat_h = _mm256_loadu_si256((const __m256i *)pat.data());
            pat_h = _mm256_blendv_epi8(avx::zero_256i, pat_h, mask_v);

            for (size_t i = 0, last = txt.size() - pat.size() + 1; i < last; ++i)
            {
                __m256i txt_h = _mm256_loadu_si256((const __m256i *)(txt.data() + i));
                txt_h = _mm256_blendv_epi8(avx::zero_256i, txt_h, mask_v);

                if (_mm256_movemask_epi8(_mm256_cmpeq_epi8(txt_h, pat_h)) == -1)
                    matches.push_back(i);
            }
        }
        else
        {
            size_t diff = pat.size() - sizeof(pat_h);
            pat_h = _mm256_loadu_si256((const __m256i *)(pat.data() + diff));

            for (size_t i = 0, last = txt.size() - pat.size() + 1; i < last; ++i)
            {
                __m256i txt_h = _mm256_loadu_si256((const __m256i *)(txt.data() + i + diff));
                if (_mm256_movemask_epi8(_mm256_cmpeq_epi8(txt_h, pat_h)) == -1)
                    matches.push_back(i);
            }
        }

        return matches;
    }
} // namespace match
