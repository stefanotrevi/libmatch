#include "kmp.hpp"
#include "zeta.hpp"

namespace match
{
    std::vector<size_t> compute_sp1(std::string_view str)
    {
        /*
        We compute the sp' function based on the z function
        */

        std::vector<size_t> z{compute_zeta(str)};
        std::vector<size_t> sp(z.size());

        /*
        now we convert the prefix-prefix to prefix-suffix, this can be done by finding the lowest 
        index i for which i + z[i] == j. 
        This can be done in linear time by overwriting, in reverse order, the sp' values.
        */
        for (size_t i = z.size() - 1; i; --i)
            sp[i + z[i] - (z[i] != 0)] = z[i];

        return sp;
    }

    std::vector<size_t> compute_sp(std::string_view str)
    {
        /*
        The sp function is similar to the sp' function, but we must be sure to assign the proper 
        value to indexes for which otherwise sp' would be smaller due to the stronger condition.
        Note how it is completely pointless to compute sp from z, as it takes longer than computing 
        sp' and always performs worse (or at most identical) to sp' during search. 
        */
        std::vector<size_t> sp{compute_sp1(str)};

        for (size_t i = sp.size() - 2; i; --i)
            if (sp[i + 1] > sp[i])
                sp[i] = sp[i + 1] - 1;

        return sp;
    }

    MatchVec kmp(std::string_view txt, std::string_view pat)
    {
        /*
        The Knuth-Morris-Pratt algorithm is a smart worst-case linear time matching algorithm.
        First, we preprocess the pattern, and we compute an array ps such that 
        sp[i] == max_l(pat[0:l] == pat[i-l:i]), (i.e. the position at which starts the longest 
        suffix of txt[0:i] which is also a prefix of txt).
        ps can be "strenghtened" by also ensuring that pat[l] != pat[i], so the same mismatch
        won't occur again, this is especially useful over binary strings as it guarantees a match
        also for the next character (though we still need to perform the comparison).
        With this knowledge, we always increase i by 1 every time we do a comparison, and we never
        need to decrease it, since the array ps tells us if we have a partial match at i, and how 
        long it is. 
        */

        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        std::vector<size_t> sp{compute_sp1(pat)};

        for (size_t i = 0, j = 0; i < txt.size();)
        {
            // we check for a match with the remaining characters
            while (j < pat.size() && pat[j] == txt[i])
            {
                ++i;
                ++j;
            }

            if (j == pat.size())
                matches.push_back(i - j);

            // the next candidate matches already with sp[j - 1] characters
            if (j)
                j = sp[j - 1];
            else
                ++i;
        }

        return matches;
    }

    using SPRT = std::pair<std::vector<size_t>, std::array<uint8_t, MAX_ALPHABET_SZ>>;

    static SPRT compute_sp2(std::string_view str)
    {
        /*
        We do not directly find the longest prefix-suffix, but the longest prefix-prefix (Z function), 
        and then we use the Z function to compute prefix-suffix. 
        */
        SPRT sp;
        size_t alpha_sz = 0;

        // reduce alphabet size
        sp.second.fill(NOT_ALPHABET_ID);
        for (size_t i = 0; i < str.size(); ++i)
            if (sp.second[str[i]] == NOT_ALPHABET_ID)
                sp.second[str[i]] = alpha_sz++;
        sp.first.resize(str.size() * alpha_sz);

        std::vector<size_t> z(compute_zeta(str));

        // use Z to compute sp'[i][x]
        for (size_t i = z.size() - 1; i; --i)
            if (z[i])
            {
                size_t j = i + z[i] - 1;
                uint8_t x = sp.second[str[z[i]]];

                sp.first[j * alpha_sz + x] = z[i] + 1;
            }
        // sp'[i][str[0]] must be set to 1!
        for (size_t i = sp.second[str[0]]; i < sp.first.size(); i += alpha_sz)
            sp.first[i] += (sp.first[i] == 0);

        return sp;
    }

    MatchVec kmp_rt(std::string_view txt, std::string_view pat)
    {
        /*
        The real-time version of KMP extends the sp' values to guarantee, for each symbol x in the 
        alphabet, that txt[sp'[i][x]] == x, or 0 if there is no such position. 
        This somewhat remembers the Bad Character Rule of the Boyer-Moore algorithm.
        So:
        1. With sp[i] after a realignment we might have the same mismatch again.
        2. With sp'[i], the mismatch won't be the same, but it still needs to be checked again 
        (except in a binary alphabet).
        3. With sp'[i][x], we will never have to check again a mismatch, so every character of txt
        is considered only once, therefore we have a real-time algorithm.
        */

        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        SPRT sp{compute_sp2(pat)};
        size_t alpha_sz = sp.first.size() / pat.size();
        size_t i = 0;
        size_t j = 0;

        while (i < txt.size())
        {
            // we check for a match with the remaining characters
            while (j < pat.size() && pat[j] == txt[i])
            {
                ++i;
                ++j;
            }

            if (j == pat.size())
                matches.push_back(i - j);

            uint8_t x = sp.second[txt[i]];

            if ((j != 0) & (x != NOT_ALPHABET_ID))
                j = sp.first[(j - 1) * alpha_sz + x];
            else
                j = 0;
            ++i;
        }
        if (j == pat.size())
            matches.push_back(i - j);

        return matches;
    }

} // namespace match
