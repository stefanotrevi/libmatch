#include "align.hpp"
#include "string_utils.hpp"
#include <algorithm>
#include <iostream>
#include <vector>

namespace align
{
    std::string apply(std::string_view str1, std::string_view str2, std::string_view edit)
    {
        std::string res;

        for (size_t i = 0, j = 0, k = 0; i < edit.size(); ++i)
            switch (edit[i])
            {
            case 'm':
                res.push_back(str1[j]);
                ++j;
                ++k;
                break;
            case 's':
                res.push_back(str2[k]);
                ++j;
                ++k;
                break;
            case 'd': ++j; break;
            case 'i':
                res.push_back(str2[k]);
                ++k;
                break;
            default: break;
            }

        return res;
    }

    static uint64_t exponential_rec(std::string_view s1, std::string_view s2, std::string &edit,
                                    uint64_t cost)
    {
        /*
        The algorithm works as follows:
        If one of the two strings is empty, we do insertions until the other string is also 
        empty. This is the base case of the recursion.
        Otherwise, if the last two characters are equal, then we do a match, if they are different 
        we do a substitution, in both cases we shorten both strings by 1 character and align the 
        remainders. 
        The final edit-string will be written backwards
        */

        // base case, we can only do insertions
        if (s1.empty() || s2.empty())
        {
            for (size_t i = 0, sz = s1.size() | s2.size(); i < sz; ++i, ++cost)
                edit.push_back('i');
            return cost;
        }

        std::string edit_old{edit};
        std::string edit_alt{edit};
        uint64_t cost_old = cost;
        uint64_t cost_alt = cost;

        // try a match
        if (s1.back() == s2.back())
        {
            edit.push_back('m');
            cost = exponential_rec(s1.substr(0, s1.size() - 1), s2.substr(0, s2.size() - 1), edit,
                                   cost_old);
        }
        // try a substitution
        else
        {
            edit.push_back('s');
            cost = exponential_rec(s1.substr(0, s1.size() - 1), s2.substr(0, s2.size() - 1), edit,
                                   cost_old + 1);
        }

        // try an insertion
        edit_alt.push_back('i');
        cost_alt = exponential_rec(s1.substr(0, s1.size()), s2.substr(0, s2.size() - 1), edit_alt,
                                   cost_old + 1);
        if (cost_alt < cost)
        {
            edit = edit_alt;
            cost = cost_alt;
        }
        edit_alt = edit_old;

        // try a deletion
        edit_alt.push_back('d');
        exponential_rec(s1.substr(0, s1.size() - 1), s2.substr(0, s2.size()), edit_alt,
                        cost_old + 1);

        if (cost_alt < cost)
        {
            edit = edit_alt;
            cost = cost_alt;
        }

        return cost;
    }

    std::string exponential(std::string_view s1, std::string_view s2)
    {
        /*
        Naive recursive exponential-time algorithm to compute an optimal string alignment.
        Given strings s1 and s2, it returns the optimal (i.e. minimum Levensthein distance)
        edit-string which aligns s1 with s2.
        The edit string contains the following characters:
        'm' -> match a character (i.e. do nothing)
        'i' -> insert a character
        'd' -> delete a character
        's' -> substitute a character
        */
        std::string edit;

        exponential_rec(s1, s2, edit, 0);
        std::ranges::reverse(edit);

        return edit;
    }

    template<typename T>
    static void print_matrix(std::ostream &os, const std::vector<T> &v, size_t cols)
    {
        os << '\n';
        for (size_t i = 0, rows = v.size() / cols; i < rows; ++i)
        {
            for (size_t j = 0; j < cols; ++j)
                os << v[i * cols + j] << '\t';
            os << '\n';
        }
        os << '\n';
    }

    struct Entry
    {
        uint64_t cost;
        char op;
        size_t prev_i;
        size_t prev_j;
    };

    std::ostream &operator<<(std::ostream &os, const Entry &e)
    {
        return os << '(' << e.cost << ", " << e.op << ", " << e.prev_i << ", " << e.prev_j << ')';
    }

    std::string dynamic(std::string_view s1, std::string_view s2)
    {
        /*
        The dynamic programming method improves over the naive/brute-forcing one by using a matrix
        to store optimal alignments for partial strings.
        */
        std::string edit;
        size_t rows = s1.size() + 1;
        size_t cols = s2.size() + 1;
        std::vector<Entry> matrix(rows * cols);

        // compute alignment for first element
        matrix[0] = {0, 'm', s1.size(), s2.size()};
        // compute alignment for the first row
        for (size_t j = 1; j < cols; ++j)
            matrix[j] = {j, 'i', s1.size(), j - 1};
        // compute alignment for the first column
        for (size_t i = 1; i < rows; ++i)
            matrix[i * cols] = {i, 'd', i - 1, s2.size()};

        // align remaining elements
        for (size_t i = 1; i < rows; ++i)
            for (size_t j = 1; j < cols; ++j)
            {
                bool equal = s1[i - 1] == s2[j - 1];
                size_t sub_mat_c = matrix[(i - 1) * cols + (j - 1)].cost + !equal;
                size_t ins_c = matrix[i * cols + (j - 1)].cost + 1;
                size_t del_c = matrix[(i - 1) * cols + j].cost + 1;

                if (sub_mat_c < ins_c & sub_mat_c < del_c)
                    matrix[i * cols + j] = {sub_mat_c, equal ? 'm' : 's', i - 1, j - 1};
                else if (ins_c < del_c)
                    matrix[i * cols + j] = {ins_c, 'i', i, j - 1};
                else
                    matrix[i * cols + j] = {del_c, 'd', i - 1, j};
            }

        // backtrack the best path
        size_t i = s1.size();
        size_t j = s2.size();
        while (i != 0 & j != 0)
        {
            const auto &e = matrix[i * cols + j];

            edit.push_back(e.op);
            i = e.prev_i;
            j = e.prev_j;
        }
        // one of the two won't be executed
        while (i)
            edit.push_back(matrix[i--].op);
        while (j)
            edit.push_back(matrix[j--].op);


        std::ranges::reverse(edit);

        return edit;
    }

} // namespace align
