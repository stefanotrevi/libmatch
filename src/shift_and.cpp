#include "shift_and.hpp"

#include <boost/dynamic_bitset.hpp>

namespace match
{
    MatchVec shift_and(std::string_view txt, std::string_view pat)
    {
        /*
        The shift-and algorithm uses a "dense" data structure, i.e. bit arrays,
        to find matches. We compute a matrix M such that M[i][j] = txt[i] == pat[j].
        Clearly, when i == pat.size() - 1, M[i][j] will tell us if we have a match.
        For each character x in the alphabet, we also create an array U[x] such that 
        U[x][i] = pat[i] == x. Then:
        1. We set the first row of M to all 0, except possibly M[0][0] if pat[0] == txt[0].
        2. Every other row is computed from the previous one, by shifting all its bits down
        by one position, and bitwise-anding them with the array U[x]. 
        */
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        // compute the U arrays
        std::array<boost::dynamic_bitset<>, MAX_ALPHABET_SZ> U;

        U.fill(boost::dynamic_bitset(pat.size()));

        // boost dynamic_bitsets are little-endian, be careful!
        for (size_t i = 0, last = pat.size() - 1; i < U.size(); ++i)
            for (size_t j = 0; j < pat.size(); ++j)
                U[i][last - j] = (char)i == pat[j];

        // incrementally compute the rows of M
        boost::dynamic_bitset row(pat.size());

        for (size_t i = 0, last = pat.size() - 1; i < txt.size(); ++i)
        {
            row >>= 1;
            row[last] = 1;
            row &= U[txt[i]];
            if (row[0])
                matches.push_back(i - last);
        }

        return matches;
    }

    MatchVec shift_and_v2(std::string_view txt, std::string_view pat)
    {
        /*
        The shift-and algorithm uses a "dense" data structure, i.e. bit arrays,
        to find matches. We compute a matrix M such that M[i][j] = pat[0:i] == txt[j-i:j].
        Clearly, when i == pat.size() - 1, M[i][j] will tell us if we have a match.
        For each character x in the alphabet, we also create an array U[x] such that 
        U[x][i] = pat[i] == x. Then:
        1. We set the first column of M to all 0, except possibly M[0][0] if pat[0] == txt[0].
        2. Every other column is computed from the previous one, by shifting all its bits down
        by one position, and bitwise-anding them with the array U[x]. 
        */
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        // compute the alphabet
        std::vector<char> alphabet;
        std::array<uint8_t, MAX_ALPHABET_SZ> alphabet_map;

        alphabet_map.fill(NOT_ALPHABET_ID);
        for (auto &&c : pat)
            if (alphabet_map[c] == NOT_ALPHABET_ID)
            {
                alphabet_map[c] = alphabet.size();
                alphabet.push_back(c);
            }

        // compute the U arrays
        std::vector<boost::dynamic_bitset<>> U(pat.size(), boost::dynamic_bitset(pat.size()));

        for (size_t i = 0; i < alphabet.size(); ++i)
            for (size_t j = 0; j < pat.size(); ++j)
                U[i][j] = alphabet[i] == pat[j];

        // incrementally compute the columns of M
        boost::dynamic_bitset col(pat.size());

        col[0] = txt[0] == pat[0];
        for (size_t i = 0; i < txt.size(); ++i)
        {
            if (alphabet_map[txt[i]] != NOT_ALPHABET_ID)
            {
                col <<= 1;
                col[0] = 1;
                col &= U[alphabet_map[txt[i]]];
                if (col[pat.size() - 1])
                    matches.push_back(i);
            }
            else
                col.reset();
        }

        return matches;
    }
} // namespace match
