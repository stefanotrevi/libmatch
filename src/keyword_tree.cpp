#include "keyword_tree.hpp"

#include <algorithm>
#include <iostream>
#include <queue>

namespace match // KeywordTreeNode
{
    KeywordTreeNode::KeywordTreeNode(KeywordTreeNode *dad, char e_lab) :
        father{dad}, depth{dad->depth + 1}, e_label{e_lab}
    {}

    void KeywordTreeNode::insert_no_check(char c) { child.emplace_front(this, c); }

    bool KeywordTreeNode::is_last_son() const
    {
        return father == nullptr || this == &this->father->child.back();
    }

    void KeywordTreeNode::sort()
    {
        child.sort([](auto &&x, auto &&y) { return x.e_label < y.e_label; });

        for (auto &&c : child)
            c.sort();
    }

    KeywordTreeNode *KeywordTreeNode::find(char c)
    {
        auto son = std::ranges::find_if(child, [c](auto &&x) { return x.e_label == c; });

        return son == child.end() ? nullptr : &*son;
    }

    const KeywordTreeNode *KeywordTreeNode::find(char c) const
    {
        auto son = std::ranges::find_if(child, [c](auto &&x) { return x.e_label == c; });

        return son == child.end() ? nullptr : &*son;
    }

    bool KeywordTreeNode::contains(char c) const { return this->find(c) != nullptr; }

    void KeywordTreeNode::insert(char c)
    {
        if (!this->contains(c))
            this->insert_no_check(c);
    }

    bool KeywordTreeNode::is_leaf() const { return child.empty(); }

    bool KeywordTreeNode::is_output() const { return label != NOT_OUTPUT_LABEL; }

    size_t KeywordTreeNode::get_depth() const { return depth; }

    size_t KeywordTreeNode::get_label() const { return label; }

    const KeywordTreeNode *KeywordTreeNode::flink() const { return fail_link; }

    const KeywordTreeNode *KeywordTreeNode::olink() const { return out_link; }

    std::ostream &operator<<(std::ostream &os, const KeywordTreeNode &node)
    {
        if (node.is_output())
            os << ' ' << node.label;
        os << '\n';

        for (auto &&c : node.child)
        {
            for (size_t i = node.depth - 1; i < node.depth; --i)
            {
                const KeywordTreeNode *aux = &node;

                for (size_t j = 0; j < i; ++j)
                    aux = aux->father;
                os << (aux->is_last_son() ? "  " : "| ");
            }
            os << c.e_label << " \\" << c;
        }

        return os;
    }

    ////////////////
} // namespace match

namespace match // KeywordTree
{
    KeywordTree::~KeywordTree()
    {
        // The default destructor is recursive and causes stack overflow for big trees
        for (KeywordTreeNode *node = &root, *father = &root;;)
        {
            while (!node->child.empty())
                node = &node->child.front();

            father = node->father;
            if (!father)
                break;
            father->child.pop_front();
            node = father;
        }
    }

    void KeywordTree::insert(std::string_view pat, size_t id)
    {
        KeywordTreeNode *node = &root;
        KeywordTreeNode *out = &root;

        for (auto &&c : pat)
        {
            KeywordTreeNode *son = node->find(c);

            if (son == nullptr)
            {
                ++num_nodes;
                node->insert_no_check(c);
                son = &node->child.front();
                son->out_link = out;
            }
            else if (son->is_output())
                out = son;
            node = son;
        }
        node->label = id;
    }

    void KeywordTree::compute_fail_links()
    {
        /*
        To build the failure links in linear time, we must compute them breadth-first:
        1. fl[root] = nullptr.
        2. fl[root.child] = root.
        3. The fl[root.{n}.child] =
            a. fl[root.{n-1}.child] + 1 if fl[root.{n-1}.child] has a child with the same label
            b. fl[...fl[root.{n-1}.child]...] until we either reach the root or a child with the 
            same label.
        */
        std::queue<KeywordTreeNode *> queue;

        root.fail_link = &root;
        for (auto &&son : root.child)
        {
            son.fail_link = &root;
            queue.push(&son);
        }

        while (!queue.empty())
        {
            KeywordTreeNode *node = queue.front();
            queue.pop();

            for (auto &&son : node->child)
            {
                auto flink = node->fail_link;

                son.fail_link = flink->find(son.e_label);
                while (flink != &root && !son.fail_link)
                {
                    flink = flink->fail_link;
                    son.fail_link = flink->find(son.e_label);
                }
                if (!son.fail_link)
                    son.fail_link = &root;

                queue.push(&son);
            }
        }
    }

    const KeywordTreeNode *KeywordTree::get_root() const { return &root; }

    void KeywordTree::sort() { root.sort(); }
    size_t KeywordTree::size() const { return num_nodes; }

    std::ostream &operator<<(std::ostream &os, const KeywordTree &tree)
    {
        return os << "\n\\" << *tree.get_root();
    }

} // namespace match
