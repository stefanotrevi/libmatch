#include "boyer_moore.hpp"
#include <array>
#include <map>

namespace match
{
    using GSR = std::pair<std::vector<size_t>, std::vector<size_t>>; // {L function, l function}

    /*
    Three different data structures for the Bad Character Rule:
    - BCR_v1: a map (generally a BST) where the entry {index, symbol} contains the BCR.
    Space: Omega(alphabet.size() * pattern.size()), lookup time: O(1)
    - BCR_v2: a pair of an array containing the id of each alphabet symbol, so we can reduce the 
    alphabet size to the characters actually present in the pattern, and a vector representing 
    Space: O(alphabet.size() * pattern.size()), lookup time: O(log(pattern.size()))
    a 2D matrix containing the entry [index][id[symbol]] contains the BCR
    - BCR_v3: an adjacency list where the bucket id[symbol] contains all the occurences of the 
    symbol in the pattern.
    Space: O(pattern.size()), lookup time: O(log(pattern.size()))/O(pattern.size()) depending on the 
    search strategy (the vector is sorted in descending order).
    */
    using BCR_v1 = std::map<std::pair<size_t, char>, size_t>;
    using BCR_v2 = std::pair<std::array<uint8_t, MAX_ALPHABET_SZ>, std::vector<size_t>>;
    using BCR_v3 =
        std::pair<std::array<uint8_t, MAX_ALPHABET_SZ>, std::vector<std::vector<size_t>>>;

    static BCR_v1 build_bcr_v1(std::string_view str)
    {
        BCR_v1 bcr;
        std::array<bool, MAX_ALPHABET_SZ> alphabet_map{};
        std::vector<char> alphabet;

        // reduce alphabet space
        for (auto &&c : str)
            if (!alphabet_map[c])
            {
                alphabet.push_back(c);
                alphabet_map[c] = true;
            }

        for (auto &&c : alphabet)
            for (size_t i = 0, rightmost = MAX_SIZE; i < str.size(); ++i)
            {
                if (rightmost != MAX_SIZE && c != str[i])
                    bcr[{i, c}] = rightmost;
                if (str[i] == c)
                    rightmost = i;
            }

        return bcr;
    }

    static BCR_v2 build_bcr_v2(std::string_view str)
    {
        BCR_v2 bcr;
        std::vector<char> alphabet;

        bcr.first.fill(NOT_ALPHABET_ID);
        // reduce alphabet space
        for (size_t i = 0, c_idx = 0; i < str.size(); ++i)
            if (bcr.first[str[i]] == NOT_ALPHABET_ID)
            {
                alphabet.push_back(str[i]);
                bcr.first[str[i]] = (uint8_t)c_idx++;
            }

        bcr.second.resize(str.size() * alphabet.size());
        for (size_t i = 0; i < alphabet.size(); ++i)
        {
            size_t r = 0;

            while (str[r] != alphabet[i])
                ++r;

            for (size_t j = r + 1; j < str.size(); ++j)
            {
                bcr.second[j * alphabet.size() + i] = r;
                if (str[j] == alphabet[i])
                    r = j;
            }
        }

        return bcr;
    }

    static BCR_v3 build_bcr_v3(std::string_view str)
    {
        BCR_v3 bcr;
        std::vector<char> alphabet;

        bcr.first.fill(NOT_ALPHABET_ID);
        // reduce alphabet space
        for (size_t i = 0, c_idx = 0; i < str.size(); ++i)
            if (bcr.first[str[i]] == NOT_ALPHABET_ID)
            {
                alphabet.push_back(str[i]);
                bcr.first[str[i]] = (uint8_t)c_idx++;
            }

        for (size_t i = 0, c_idx = 0; i < str.size(); ++i)
            if (bcr.first[str[i]] == NOT_ALPHABET_ID)
                bcr.first[str[i]] = (uint8_t)c_idx++;


        bcr.second.resize(alphabet.size());
        for (size_t i = 0; i < str.size(); ++i)
            bcr.second[bcr.first[str[i]]].push_back(i);

        return bcr;
    }

    static GSR build_gsr(std::string_view str)
    {
        GSR gsr{std::vector<size_t>(str.size()), std::vector<size_t>(str.size())};

        /* 
        1.  We compute the Z function backwards (aka N function), obtaining:
            zb[i] == max_j(str[n-j:n] == str[i-j:i])
        */
        std::vector<size_t> zb(str.size());
        size_t last = str.size() - 1;
        size_t l = last;
        size_t r = last;

        for (size_t i = last - 1; i != MAX_SIZE; --i)
        {
            if (i > l)
                zb[i] = zb[last - (r - l) + (i - l)];
            if (i - zb[i] <= l)
            {
                if (i > l)
                    zb[i] = i - l;
                while (zb[i] <= i && str[last - zb[i]] == str[i - zb[i]])
                    ++zb[i];
                r = i;
                l = i - zb[i];
            }
        }

        /*
        2.  We use the N function to compute the L function, obtaining: 
            gsr.first[i] == max_j(str[j-(n-i):j] == str[i:n] && str[j-(n-i)-1] != str[i-1])
            A good candidate is gsr.first[n - zb[i]]. 
            By computing it left-to-right, we always have the rightmost i.
        */
        for (size_t i = 0; i < str.size(); ++i)
            if (zb[i])
                gsr.first[str.size() - zb[i]] = i + 1;

        /*
        3. We use the N function to compute the l function, obtaining:
            gsr.second[i] == max_j(i < n - j && str[0:j] == str[n-j:n])
            Which is the longest prefix of str which is also a suffix of str[i:n].
            Whenever zb[i] = i, it means we have a prefix-suffix, and if we scan left-to-right, 
            the last such value will always be the biggest one.
        */
        for (size_t i = 0, rightmost = 0; i < str.size(); ++i)
        {
            gsr.second[last - i] = rightmost;
            if (zb[i] == i + 1)
                rightmost = i + 1;
        }

        return gsr;
    }

    MatchVec boyer_moore(std::string_view txt, std::string_view pat)
    {
        /*
        The Boyer-Moore algorithm is a complex worst-case quadratic time matching algorithm, but 
        often should be faster than, say, the KMP algorithm (at least in theory). 
        It requires more preprocessing and potentially a lot more space, but it can achieve 
        a complexity of up to O(txt.size() / pat.size()) under certain conditions.
        There are three preprocessing steps (assume n == pat.size()):
        1.  Bad Character Rule (BCR): 
                bcr[i][c] == max_j(j < i && pat[j] == c)
        2.  (Strong) Good Suffix Rule (GSR): 
                gsr.first[i] == max_j(j < i && pat[j-(n-i):j] == pat[i:n] && pat[i-1] != pat[j-(n-i)-1])
                gsr.second[i] == max_j(i < n - j && pat[0:j] == pat[n-j:n])
        Then we start checking, in reverse order, if pat[0:n] == txt[i:i+n].
        Whenever there is a mismatch between txt[i] and pat[j], since bcr[j][txt[i]] tells us the 
        rightmost occurence of the txt[i], we can increment i to align txt[i] with pat[bcr[j][txt[i]]].
        Also, since gsr.first[j] tells us the rightmost occurence of the matched characters, we can 
        increment i to align txt[i] with pat[gsr[j] - (n - j)]. 
        Finally, gsr.second[j] is useful when we find an occurence or whenever gsr.first[j] == 0, and
        works similarly to the KMP precomputation. 
        When we need to shift the pattern, we choose the maximum shift between the BCR and the GSR.
        
        NOTE:
        This is not actually the original Boyer-Moore algorithm, but it is adapted to always 
        operate in linear time. The change is very simple: when there is a match, there is no
        need to check the first pat[0:gsr.second[1]] characters!
        */

        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        BCR_v2 bcr{build_bcr_v2(pat)};
        GSR gsr{build_gsr(pat)};
        size_t i = 0;
        size_t lim = MAX_SIZE;
        size_t last = pat.size() - 1;
        size_t sz = txt.size() - pat.size() + 1;
        size_t alpha_sz = bcr.second.size() / pat.size(); // only for BCR_v2

        while (i < sz)
        {
            size_t j = last;

            while (j != lim && pat[j] == txt[i + j])
                --j;
            if (j == lim)
            {
                // when there is a match, we just align to the biggest prefix-suffix
                matches.push_back(i);
                lim = gsr.second[0] - 1;
                i += pat.size() - gsr.second[0];
            }
            else
            {
                size_t x = bcr.first[txt[i + j]];
#define BCR_VERSION 2
#if BCR_VERSION == 1
                size_t bcr_k = 0;
                if (auto bcr_kp = bcr.find({j, txt[i + j]}); bcr_kp != bcr.end())
                    bcr_k = bcr_kp->second;
#elif BCR_VERSION == 2
                size_t bcr_k = x == NOT_ALPHABET_ID ? 0 : bcr.second[j * alpha_sz + x];
#else // we use BCR_v3 (TODO)
#endif
#undef BCR_VERSION
                size_t gsr_k = j == last ? last : gsr.first[j + 1];

                if (gsr_k)
                {
                    lim = MAX_SIZE;
                    i += std::max(pat.size() - gsr_k, j - bcr_k);
                }
                else
                {
                    gsr_k = j == last ? 0 : gsr.second[j + 1];
                    lim = gsr_k - 1;
                    i += pat.size() - gsr_k;
                }
            }
        }

        return matches;
    }


    /*
    The following versions only implement the BCR, and serve as a comparison of some of the 
    possible implementations
    */

    MatchVec boyer_moore_v1(std::string_view txt, std::string_view pat)
    {
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        BCR_v1 bcr{build_bcr_v1(pat)};

        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz;)
        {
            size_t j = pat.size() - 1;

            while (j != ~0ULL && pat[j] == txt[i + j])
                --j;
            if (j == ~0ULL)
                matches.push_back(i++);
            else
            {
                auto may_k = bcr.find({j, txt[i + j]});

                i += may_k == bcr.end() ? j + 1 : j - may_k->second;
            }
        }

        return matches;
    }

    MatchVec boyer_moore_v2(std::string_view txt, std::string_view pat)
    {
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        BCR_v2 bcr{build_bcr_v2(pat)};
        size_t alpha_sz = bcr.second.size() / pat.size();

        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz;)
        {
            size_t j = pat.size() - 1;

            while (j != ~0ULL && pat[j] == txt[i + j])
                --j;
            if (j == ~0ULL)
                matches.push_back(i++);
            else
            {
                size_t c_idx = bcr.first[txt[i + j]];
                size_t k = c_idx == NOT_ALPHABET_ID ? 0 : bcr.second[j * alpha_sz + c_idx];

                i += j - k + (j == 0);
            }
        }

        return matches;
    }

    MatchVec boyer_moore_v3(std::string_view txt, std::string_view pat)
    {
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        BCR_v3 bcr{build_bcr_v3(pat)};

        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz;)
        {
            size_t j = pat.size() - 1;

            while (j != ~0ULL && pat[j] == txt[i + j])
                --j;
            if (j == ~0ULL)
                matches.push_back(i++);
            else
            {
                size_t x = bcr.first[txt[i + j]];
                size_t k = 0;

                if (x != NOT_ALPHABET_ID)
                {
                    size_t l;

                    for (l = 0; l < bcr.second[x].size(); ++l)
                        if (bcr.second[x][l] >= j)
                            break;
                    if (l != 0)
                        k = bcr.second[x][l - 1];
                }
                i += j - k + (j == 0);
            }
        }

        return matches;
    }
} // namespace match
