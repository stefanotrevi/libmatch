#include "zeta.hpp"

namespace match
{
    std::vector<size_t> compute_zeta(std::string_view str)
    {
        std::vector<size_t> z(str.size());

        size_t l = 0;
        size_t r = 0;

        for (size_t i = 1; i < str.size(); ++i)
        {
            if (i < r) // pat[l:r] == pat[0:(r-l)], so z[i] is at least z[i - l]
                z[i] = z[i - l];
            if (i + z[i] >= r) // we don't know what lies beyond r
            {
                if (i < r)
                    z[i] = r - i;
                while (z[i] < str.size() - i && str[z[i]] == str[i + z[i]])
                    ++z[i];
                l = i; // update the boundaries
                r = i + z[i];
            }
        }

        return z;
    }

    MatchVec zeta(std::string_view txt, std::string_view pat)
    {
        /*
        The Zeta algorithm is, in spirit, the base of the KMP algorithm. 
        Instead of using the longest suffix-prefix, we compute the longest prefix-prefix: 
        for each i, z[i] == max_j(pat[0:j] == pat[i:i+j]).
        If we compute z over pat++$++txt, where $ is a new symbol which is never present in pat nor 
        in txt, we will be able to find matches in linear time, as whenever z[i] == pat.size(), we 
        will have a match.
        */
        MatchVec matches;

        if (pat.size() == 0 || txt.size() == 0 || txt.size() < pat.size())
            return matches;

        std::vector<size_t> z{compute_zeta(pat)};
        size_t l = 0;
        size_t r = 0;

        // we keep computing z over pat++$++txt, without using extra memory
        for (size_t i = 0, sz = txt.size() - pat.size() + 1; i < sz; ++i)
        {
            size_t z_i = 0;

            if (i < r)
                z_i = z[i - l];
            if (i + z_i >= r)
            {
                if (i < r)
                    z_i = r - i;
                while (z_i < pat.size() && pat[z_i] == txt[i + z_i])
                    ++z_i;
                l = i;
                r = i + z_i;
            }
            if (z_i == pat.size())
                matches.push_back(i);
        }

        return matches;
    }
} // namespace match
