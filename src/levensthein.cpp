#include "levensthein.hpp"
#include "intrinsics.h"

namespace align
{
    uint64_t levensthein0(std::string_view str)
    {
        uint64_t cost = 0;

        for (size_t i = 0; i < str.size(); ++i)
            if (str[i] == 'm')
                ++cost;

        return str.size() - cost;
    }


    uint64_t levensthein1(std::string_view str)
    {
        uint64_t cost = 0;

        for (auto &&c : str)
        {
            switch (c)
            {
            case 's':
            case 'i':
            case 'd': ++cost;
            default: break;
            }
        }

        return cost;
    }

    uint64_t levensthein2(std::string_view str)
    {
        uint64_t cost = 0;

        for (auto &&c : str)
            cost += (c == 's') | (c == 'i') | (c == 'd');

        return cost;
    }

    uint64_t levensthein3(std::string_view str)
    {
        uint64_t cost = 0;
        static constexpr uint8_t cost_map[256]{
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 32
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 48
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 64
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 80
            0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, // 96
            0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 112
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 128
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 144
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 160
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 176
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 192
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 208
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 224
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 240
        };

        for (auto &&c : str)
            cost += cost_map[(uint8_t)c];

        return cost;
    }

    uint64_t levensthein_nocheck(std::string_view str)
    {
        uint64_t cost = 0;

        for (auto &&c : str)
            if (c != 'm')
                ++cost;


        return cost;
    }

    uint64_t levensthein_nocheck_v2(std::string_view str)
    {
        static constexpr size_t STEP = sizeof(__m256i) / sizeof(str[0]);
        uint64_t cost = 0;
        __m256i match = _mm256_set1_epi8('m');
        size_t rem = str.size() % STEP;
        size_t sz = str.size() - rem;

        for (size_t i = 0; i < sz; i += STEP)
        {
            __m256i v = _mm256_loadu_si256((const __m256i *)(str.data() + i));
            int msk = _mm256_movemask_epi8(_mm256_cmpeq_epi8(match, v));

            cost += _popcnt32(~msk);
        }

#pragma novector
        for (size_t i = sz; i < str.size(); ++i)
            cost += str[i] != 'm';

        return cost;
    }

    uint64_t levensthein(std::string_view str) { return levensthein_nocheck_v2(str); }

} // namespace align
