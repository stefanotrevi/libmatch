#include "suffix_trie.hpp"

#include <algorithm>
#include <iostream>
#include <queue>

namespace match // SuffixTrieNode
{
    SuffixTrieNode::SuffixTrieNode(SuffixTrieNode *dad, char e_lab) :
        father{dad}, depth{dad->depth + 1}, e_label{e_lab}
    {}

    void SuffixTrieNode::insert_no_check(char c) { child.emplace_front(this, c); }

    bool SuffixTrieNode::is_last_son() const
    {
        return father == nullptr || this == &this->father->child.back();
    }

    void SuffixTrieNode::sort()
    {
        child.sort([](auto &&x, auto &&y) { return x.e_label < y.e_label; });

        for (auto &&c : child)
            c.sort();
    }

    SuffixTrieNode *SuffixTrieNode::find(char c)
    {
        auto son = std::ranges::find_if(child, [c](auto &&x) { return x.e_label == c; });

        return son == child.end() ? nullptr : &*son;
    }

    const SuffixTrieNode *SuffixTrieNode::find(char c) const
    {
        auto son = std::ranges::find_if(child, [c](auto &&x) { return x.e_label == c; });

        return son == child.end() ? nullptr : &*son;
    }

    bool SuffixTrieNode::contains(char c) const { return this->find(c) != nullptr; }

    void SuffixTrieNode::insert(char c)
    {
        if (!this->contains(c))
            this->insert_no_check(c);
    }

    bool SuffixTrieNode::is_leaf() const { return child.empty(); }

    bool SuffixTrieNode::is_output() const { return label != NOT_OUTPUT_LABEL; }

    size_t SuffixTrieNode::get_depth() const { return depth; }

    size_t SuffixTrieNode::get_label() const { return label; }

    const SuffixTrieNode *SuffixTrieNode::slink() const { return suf_link; }

    std::ostream &operator<<(std::ostream &os, const SuffixTrieNode &node)
    {
        if (node.is_output())
            os << ' ' << node.label;
        os << '\n';

        for (auto &&c : node.child)
        {
            for (size_t i = node.depth - 1; i < node.depth; --i)
            {
                const SuffixTrieNode *aux = &node;

                for (size_t j = 0; j < i; ++j)
                    aux = aux->father;
                os << (aux->is_last_son() ? "  " : "| ");
            }
            os << c.e_label << " \\" << c;
        }

        return os;
    }
} // namespace match

namespace match // SuffixTrie
{
    SuffixTrie::SuffixTrie(std::string_view str)
    {
        /*
        A suffix trie is the keyword tree of all the suffixes of str.
        We build a suffix trie using the incremental method, by building the suffix trie of 
        str[0:i] from the one of str[0:i-1]. 
        To do this, it is sufficient, at every step, to augment the boundary path of the tree, 
        which is the set of the output nodes.
        */
        SuffixTrieNode *fout = &root;

        for (size_t i = 0; i < str.size(); ++i)
        {
            SuffixTrieNode *node = fout, *son;

            root.label = i;
            node->insert_no_check(str[i]); // the fout node has no children
            son = &node->child.front();

            // the son takes over the father, it will also be the new final output
            son->label = node->label;
            node->label = SuffixTrieNode::NOT_OUTPUT_LABEL;
            node = node->suf_link;
            fout = son;

            while (node)
            {
                son->suf_link = node->find(str[i]);

                if (!son->suf_link) // maybe it already exists
                {
                    node->insert_no_check(str[i]);
                    son->suf_link = &node->child.front();
                }
                son = son->suf_link;
                // the son takes over the father
                son->label = node->label;
                node->label = SuffixTrieNode::NOT_OUTPUT_LABEL;
                node = node->suf_link;
            }
            son->suf_link = &root;
        }
        root.suf_link = &root;
    }

    SuffixTrie::~SuffixTrie()
    {
        // The default destructor is recursive and causes stack overflow for big trees
        for (SuffixTrieNode *node = &root, *father = &root;;)
        {
            while (!node->child.empty())
                node = &node->child.front();

            father = node->father;
            if (!father)
                break;
            father->child.pop_front();
            node = father;
        }
    }

    const SuffixTrieNode *SuffixTrie::get_root() const { return &root; }

    void SuffixTrie::sort() { root.sort(); }
    size_t SuffixTrie::size() const { return num_nodes; }

    std::ostream &operator<<(std::ostream &os, const SuffixTrie &tree)
    {
        return os << "\n\\" << *tree.get_root();
    }

} // namespace match
