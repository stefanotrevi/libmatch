#pragma once

#include "match.hpp"

namespace match
{
    std::vector<size_t> compute_zeta(std::string_view str);
    MatchVec zeta(std::string_view txt, std::string_view pat);
} // namespace match
