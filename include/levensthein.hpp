#pragma once

#include "align.hpp"

namespace align
{
    uint64_t levensthein(std::string_view str);

    // various different implementations
    uint64_t levensthein0(std::string_view str);
    uint64_t levensthein1(std::string_view str);
    uint64_t levensthein2(std::string_view str);
    uint64_t levensthein3(std::string_view str);
    uint64_t levensthein_nocheck(std::string_view str);
    uint64_t levensthein_nocheck_v2(std::string_view str);
} // namespace align
