#pragma once

#include "string_utils.hpp"
#include <ostream>
#include <string>
#include <vector>
#include <climits>

namespace match
{
    using MatchFunction = std::vector<size_t>(std::string_view str, std::string_view pat);
    using MatchVec = std::vector<size_t>;

    struct MultiMatch
    {
        size_t pos; // position at which the pattern was found
        size_t id;  // id of the pattern

        auto operator<=>(const MultiMatch &y) const = default;
    };

    inline std::ostream &operator<<(std::ostream &os, const MultiMatch &mm)
    {
        return os << '(' << mm.pos << ", " << mm.id << ')';
    }

    using MultiMatchVec = std::vector<MultiMatch>;

    static constexpr size_t MAX_ALPHABET_SZ = 1ULL << sizeof(char) * CHAR_BIT;
    static constexpr uint8_t NOT_ALPHABET_ID = 255;
    static constexpr size_t MAX_SIZE = ~0ULL;

    template<MatchFunction match_foo> class Matcher
    {
    private:
        std::string str;
        MatchVec match;

    public:
        Matcher(std::string_view text) : str{text}, match{} {}

        void find(std::string_view pattern) { match = match_foo(str, pattern); }

        const MatchVec &get_match() const { return match; }

        friend std::ostream &operator<<(std::ostream &os, const Matcher &matcher)
        {
            return os << matcher.match;
        }
    };
} // namespace match
