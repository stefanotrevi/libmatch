#pragma once

#include <string>
#include <string_view>

namespace align
{
    std::string apply(std::string_view str1, std::string_view str2, std::string_view edit);

    std::string exponential(std::string_view str1, std::string_view str2);

    std::string dynamic(std::string_view s1, std::string_view s2);
} // namespace align
