#pragma once

#include "match.hpp"

namespace match
{
    MatchVec shift_and(std::string_view str, std::string_view pat);
}
