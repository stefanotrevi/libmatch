#pragma once

#include "match.hpp"

namespace match
{
    MatchVec rabin_karp(std::string_view str, std::string_view pat);

    MatchVec rabin_karp_maxmod(std::string_view str, std::string_view pat);
}
