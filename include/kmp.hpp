#pragma once

#include "match.hpp"

namespace match
{
    std::vector<size_t> compute_sp(std::string_view str);

    std::vector<size_t> compute_sp1(std::string_view str);

    MatchVec kmp(std::string_view txt, std::string_view pat);

    MatchVec kmp_rt(std::string_view txt, std::string_view pat);
} // namespace match
