#pragma once

#include "match.hpp"

namespace match
{
    // Uses both the BCR and the SGSR, plus it is modified to always run in linear time
    MatchVec boyer_moore(std::string_view str, std::string_view pat);

    // Uses a std::map for the BCR
    MatchVec boyer_moore_v1(std::string_view str, std::string_view pat);

    // Uses an adjacency matrix for the BCR
    MatchVec boyer_moore_v2(std::string_view str, std::string_view pat);

    // Uses a sorted adjacency list for the BCR
    MatchVec boyer_moore_v3(std::string_view str, std::string_view pat);
}
