#pragma once

#include <algorithm>
#include <list>
#include <ostream>
#include <vector>
#include <string>
#include <string_view>

namespace match
{
    class SuffixTreeNode
    {
    private:
        static constexpr size_t NOT_OUTPUT_LABEL = ~0ULL;

        std::list<SuffixTreeNode> child;
        SuffixTreeNode *father{nullptr};
        SuffixTreeNode *suf_link{nullptr};
        size_t depth{0};
        size_t label{NOT_OUTPUT_LABEL};
        std::string_view e_label;

        void insert_no_check(std::string_view str);

        void extend_label(std::string_view::const_iterator str);

        bool is_last_son() const;

        void sort();

        SuffixTreeNode *find(std::string_view str);
        SuffixTreeNode *find(char c);

        friend class SuffixTree;

    public:
        SuffixTreeNode() = default;

        SuffixTreeNode(SuffixTreeNode *father, std::string_view e_label);

        const SuffixTreeNode *find(std::string_view str) const;
        const SuffixTreeNode *find(char c) const;

        bool contains(std::string_view str) const;
        bool contains(char c) const;

        void insert(std::string_view str);

        bool is_leaf() const;

        bool is_output() const;

        size_t get_depth() const;

        size_t get_label() const;

        const SuffixTreeNode *slink() const;

        friend std::ostream &operator<<(std::ostream &os, const SuffixTreeNode &node);
    };

    class SuffixTree
    {
    protected:
        SuffixTreeNode root;
        size_t num_nodes{0};
        std::string data;
        std::string_view str;

    public:
        SuffixTree() = default;

        SuffixTree(std::string_view in_str);
        ~SuffixTree();

        const SuffixTreeNode *get_root() const;

        void sort();
        size_t size() const;
    };


    std::ostream &operator<<(std::ostream &os, const SuffixTree &tree);
} // namespace match
