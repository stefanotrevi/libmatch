#pragma once

#include "keyword_tree.hpp"
#include "match.hpp"

namespace match
{
    MatchVec naive(std::string_view txt, std::string_view pat);

    template<std::ranges::range Range> MultiMatchVec naive_multi(std::string_view txt, Range pats)
    {
        /*
        To perform multiple pattern matching, we use a KeywordTree, where each edge is labeled
        with a character, and every full path in the tree corresponds to a pattern, so common 
        prefixes can be checked in parallel, as they follow the same path, and multiple pats 
        can be excluded at once.
        We assume that no pattern is a prefix of any other.
        To perform multiple matching at once, we just walk down the tree until we find a leaf
        or a mismatch.
        */

        MultiMatchVec matches;
        KeywordTree tree{pats};

        for (size_t i = 0; i < txt.size(); ++i)
        {
            const KeywordTreeNode *node = tree.get_root();

            // walk down the tree until a mismatch or a leaf
            for (size_t j = i; j < txt.size() && node; ++j)
            {
                node = node->find(txt[j]);
                if (node && node->is_output())
                    matches.push_back({.pos = i, .id = node->get_label()});
            }
        }

        return matches;
    }
} // namespace match
