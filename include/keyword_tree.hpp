#pragma once

#include <algorithm>
#include <list>
#include <ostream>
#include <vector>

namespace match
{
    class KeywordTreeNode
    {
    private:
        static constexpr size_t NOT_OUTPUT_LABEL = ~0ULL;

        std::list<KeywordTreeNode> child;
        KeywordTreeNode *father{nullptr};
        KeywordTreeNode *fail_link{nullptr};
        KeywordTreeNode *out_link{nullptr};
        size_t depth{0};
        size_t label{NOT_OUTPUT_LABEL};
        char e_label{'\0'};

        void insert_no_check(char c);

        bool is_last_son() const;

        void sort();

        friend class KeywordTree;

    public:
        KeywordTreeNode() = default;

        KeywordTreeNode(KeywordTreeNode *father, char e_label);

        KeywordTreeNode *find(char c);
        const KeywordTreeNode *find(char c) const;

        bool contains(char c) const;

        void insert(char c);

        bool is_leaf() const;

        bool is_output() const;

        size_t get_depth() const;

        size_t get_label() const;

        const KeywordTreeNode *flink() const;

        const KeywordTreeNode *olink() const;

        friend std::ostream &operator<<(std::ostream &os, const KeywordTreeNode &node);
    };

    class KeywordTree
    {
    private:
        KeywordTreeNode root;
        size_t num_nodes{0};

        void insert(std::string_view pat, size_t id);
        void compute_fail_links();

    public:
        KeywordTree() = default;

        template<std::ranges::range Range> KeywordTree(const Range &strings)
        {
            /*
            Build the Keyword Tree. We label each edge with a distinct letter, and every leaf 
            with the index of the corresponding pattern.
            */
            std::vector<std::string_view> strs;

            for (auto &&str : strings)
                strs.emplace_back(str);
            // we sort the strings so that it is easier to compute the output links.
            std::ranges::stable_sort(strs, [](auto &&x, auto &&y) { return x.size() < y.size(); });

            root.out_link = &root;
            for (size_t i = 0; i < strs.size(); ++i)
                this->insert(strs[i], i);

            this->compute_fail_links();
        }
        ~KeywordTree();

        const KeywordTreeNode *get_root() const;

        void sort();
        size_t size() const;
    };


    std::ostream &operator<<(std::ostream &os, const KeywordTree &tree);
} // namespace match
