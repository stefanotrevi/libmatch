#pragma once

#include "keyword_tree.hpp"
#include "match.hpp"

namespace match
{
    template<std::ranges::range Range>
    MultiMatchVec aho_korasick(std::string_view txt, Range patterns)
    {
        /*
        The Aho-Korasick algorithm is basically an efficient extension of KMP to multiple pattern
        matching, which allows to find all occurences of all the provided patterns O(txt.size()) 
        time, after a preprocessing that takes time and space O(sum_i(patterns[i].size())).
        We assume that no pattern is a prefix of any other.
        Depending on txt[i], we walk down the tree, and when we reach a leaf we report an occurence. 
        When a mismatch occurs at node v (i.e. no edge is labeled with txt[i]), then v.flink() 
        points to the node whose path corresponds to the longest suffix of the current path, just
        like the sp function did in KMP, so we can avoid decrementing the index without losing 
        any occurence.
        */

        MultiMatchVec matches;
        KeywordTree tree{patterns};
        const KeywordTreeNode *node = tree.get_root();

        for (size_t i = 0; i < txt.size();)
        {
            auto old = node;

            // walk down the tree until a mismatch or a leaf
            while (i < txt.size() && node)
            {
                old = node;
                node = node->find(txt[i++]);
                if (node && node->is_output())
                    matches.push_back({.pos = i - node->get_depth(), .id = node->get_label()});
            }
            node = old;
            if (node != tree.get_root())
                --i;

            node = node->flink();
            // we might have missed some occurences of shorter patterns!
            for (auto out = node->olink(); out != tree.get_root(); out = out->olink())
                matches.push_back({.pos = i - node->get_depth(), .id = node->get_label()});
        }

        return matches;
    }
} // namespace match
