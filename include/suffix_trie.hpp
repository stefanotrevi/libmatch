#pragma once

#include <algorithm>
#include <list>
#include <ostream>
#include <vector>

namespace match
{
    class SuffixTrieNode
    {
    private:
        static constexpr size_t NOT_OUTPUT_LABEL = ~0ULL;

        std::list<SuffixTrieNode> child;
        SuffixTrieNode *father{nullptr};
        SuffixTrieNode *suf_link{nullptr};
        size_t depth{0};
        size_t label{NOT_OUTPUT_LABEL};
        char e_label{'\0'};

        void insert_no_check(char c);

        bool is_last_son() const;

        void sort();

        friend class SuffixTrie;

    public:
        SuffixTrieNode() = default;

        SuffixTrieNode(SuffixTrieNode *father, char e_label);

        SuffixTrieNode *find(char c);
        const SuffixTrieNode *find(char c) const;

        bool contains(char c) const;

        void insert(char c);

        bool is_leaf() const;

        bool is_output() const;

        size_t get_depth() const;

        size_t get_label() const;

        const SuffixTrieNode *slink() const;

        friend std::ostream &operator<<(std::ostream &os, const SuffixTrieNode &node);
    };

    class SuffixTrie
    {
    protected:
        SuffixTrieNode root;
        size_t num_nodes{0};

    public:
        SuffixTrie() = default;

        SuffixTrie(std::string_view str);
        ~SuffixTrie();

        const SuffixTrieNode *get_root() const;

        void sort();
        size_t size() const;
    };


    std::ostream &operator<<(std::ostream &os, const SuffixTrie &tree);
} // namespace match
