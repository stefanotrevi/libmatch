# Enables debug mode
DEBUG := 1
# Enables Intel Compiler
INTEL_COMPILER := 1
# Enables CUDA modules
CUDA_ENABLE := 1
# Measure performance in tests
MEASURE_PERFORMANCE := 1

LIBNAME := libmatch
TARGETS_TEST := aho_korasick align boyer_moore keyword_tree kmp levensthein naive rabin_karp \
    shift_and suffix_tree suffix_trie zeta
TARGETS_NOTEST :=
TARGETS := $(TARGETS_TEST) $(TARGETS_NOTEST)
CUDA_TARGETS := 


ifeq ($(ONEAPI_ROOT), )
    INTEL_COMPILER := 0
endif

ifeq ($(CUDA_PATH), )
    ifeq ($(shell which nvcc), )
        CUDA_ENABLE := 0
    endif
endif

ifeq ($(CXX), )
    CXX := c++
    CXXFLAGS := -O3 -march=native -std=c++20
endif

ifeq ($(IXX), )
    ifeq ($(OS), Windows_NT)
        IXX := icx
        IXXFLAGS := -O3 -QxCORE-AVX2 -Qopenmp -nologo -W4 -Qstd=c++20
    else
        IXX := icpx
        IXXFLAGS := -O3 -xCORE-AVX2 -fopenmp -Wall -std=c++20
    endif
endif

ifeq ($(INTEL_COMPILER), 1)
    CXX := $(IXX)
    CXXFLAGS := $(IXXFLAGS)
endif

ifeq ($(NVCC), )
    NVCC := nvcc
    NVCCFLAGS := -O3 -arch=compute_37 -code=compute_37 -std=c++17
endif

ifeq ($(DEBUG), 1)
    ifeq ($(OS), Windows_NT)
        CXXFLAGS := -debug:inline-debug-info -nologo -Qstd=c++20 -QxCORE-AVX2
    else
        ifeq ($(INTEL_COMPILER), 1)
            CXXFLAGS := -debug inline-debug-info -std=c++20 -xCORE-AVX2 -isystem /usr/include/c++/11 -isystem /usr/include/c++/11/x86_64-generic-linux
        else
            CXXFLAGS := -g -std=c++20 -march=native -fopenmp
        endif
    endif
    CXXFLAGS += -Wall
    NVCCFLAGS := -g -std=c++17 -arch=compute_61 -code=compute_61 -ccbin=/usr/local/cuda/bin/ -forward-unknown-opts -march=native
endif

TESTPATH := test
SRCPATH := src
SRC := $(wildcard $(SRCPATH)/*.cpp $(SRCPATH)/**/*.cpp)
NVSRC := $(wildcard $(SRCPATH)/*.cu $(SRCPATH)/**/*.cu)

INCPATH := include
INC := $(wildcard $(INCPATH)/*.h $(INCPATH)/**/*.h $(INCPATH)/*.hpp $(INCPATH)/**/*.hpp)
NVINC := $(wildcard $(INCPATH)/*.cuh $(INCPATH)/**/*.cuh)

BUILDPATH := build
BINPATH := bin
LIBPATH := lib

ifeq ($(OS), Windows_NT)
    AR := lib
    ARFLAGS := -nologo
    CXXFLAGS += -Qopenmp -QMMD -Fo:$(BUILDPATH)/
    NVCCFLAGS += -MD
    OEXT := obj
    LIBEXT := lib
    LIBOUTFLAGS := /out:
    ifeq ($(CUDA_ENABLE), 1)
        LDFLAGS := -link cudart.lib
    endif
    PDB := $(SRC:.c=.pdb)
    TARGETS_TEST_COMMAND := FOR %%%%i IN (*.exe) DO %%%%i
    MKDIR = if not exist $(1) mkdir $(1)
    RM = if exist $(1) del /F /Q $(1)
    MV = mv $(1) $(2)
else
    AR := ar
    ARFLAGS := r
    CXXFLAGS += -fopenmp
    ifeq ($(CUDA_ENABLE), 1)
        LDFLAGS := -lcudart
        ifneq ($(CUDALIB), )
            LDFLAGS += -L$(CUDALIB)
        endif
    endif
    OEXT := o
    LIBEXT := a
    PDB :=
    TARGETS_TEST_COMMAND := $(TARGETS_TEST:%=./%;\n)
    MKDIR = mkdir -p $(1)
    RM = rm -rf $(1)/*
    MV = mv $(1) $(2)/
    CP = cp $(1) $(2)/
endif

ifeq ($(CUDA_ENABLE), 1)
    CXXFLAGS += -DCUDA_ENABLE
endif

ifeq ($(MEASURE_PERFORMANCE), 1)
    CXXFLAGS += -DMEASURE_PERFORMANCE
endif

CXXFLAGS += -I$(INCPATH)
NVCCFLAGS += -I$(INCPATH)

SRCNAMES := $(notdir $(SRC))
OBJ := $(SRCNAMES:%.cpp=$(BUILDPATH)/%.$(OEXT))

NVSRCNAMES := $(notdir $(NVSRC))
NVOBJ := $(NVSRCNAMES:%.cu=$(BUILDPATH)/%.$(OEXT))

LIBSNAMES := $(TARGETS)

ifeq ($(CUDA_ENABLE), 1)
    LIBSNAMES += $(CUDA_TARGETS)
endif


LIBS := $(LIBSNAMES:%=$(BUILDPATH)/%.$(OEXT))

DEP := $(OBJ:%.$(OEXT)=%.d) 
ifeq ($(CUDA_ENABLE), 1)
    DEP += $(NVOBJ:%.$(OEXT)=%.d)
endif

all: dirs testsuite $(TARGETS) library
    

clean:
	$(call RM,$(BUILDPATH))
	$(call RM,$(BINPATH))
	$(call RM,$(LIBPATH))


dirs:
	$(call MKDIR,$(BUILDPATH))
	$(call MKDIR,$(BINPATH))
	$(call MKDIR,$(LIBPATH))

ifeq ($(OS), Windows_NT)
testsuite:
	@echo @echo off > $(BINPATH)/test.bat
	@echo $(TARGETS_TEST_COMMAND) >> $(BINPATH)/test.bat
else
testsuite:
	@echo -e "#!/bin/sh\n$(TARGETS_TEST_COMMAND)" > $(BINPATH)/test.sh
endif

library: $(TARGETS)
	$(AR) $(ARFLAGS) $(LIBOUTFLAGS)$(LIBPATH)/$(LIBNAME).$(LIBEXT) $(LIBS)


###################### BEGIN RULES ######################

aho_korasick:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/keyword_tree.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

align:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/levensthein.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

boyer_moore:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/naive.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

keyword_tree:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

kmp:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/naive.$(OEXT) $(BUILDPATH)/zeta.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

levensthein:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

naive:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/keyword_tree.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

rabin_karp:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/naive.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

shift_and:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/naive.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

suffix_tree:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

suffix_trie:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

zeta:  %: $(BUILDPATH)/%.$(OEXT) $(BUILDPATH)/test_%.$(OEXT) $(BUILDPATH)/naive.$(OEXT)
	$(CXX) $(CXXFLAGS) $^ -o $(BINPATH)/$@ $(LDFLAGS)

ifeq ($(CUDA_ENABLE), 1)

endif

###################### END RULES ######################

-include $(DEP)

$(BUILDPATH)/test_%.$(OEXT): $(TESTPATH)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILDPATH)/%.$(OEXT): $(SRCPATH)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILDPATH)/%.$(OEXT): $(SRCPATH)/**/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

ifeq ($(CUDA_ENABLE), 1)
$(BUILDPATH)/%.$(OEXT): $(SRCPATH)/%.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@

$(BUILDPATH)/%.$(OEXT): $(SRCPATH)/**/%.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@
endif
